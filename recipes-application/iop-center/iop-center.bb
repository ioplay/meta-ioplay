DESCRIPTION = "Create your own individual media player controlled by physical inputs without coding."
HOMEPAGE = "https://gitlab.com/ioplay/iop-center"

LICENSE = "AGPL-3.0+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b59693519b9a105a93f7a01937ce1cae"

PACKAGES =+ " \
  rfid-player \
  rfid-reader \
  test-player \
"

RDEPENDS_rfid-player += " \
  mpd \
  udev-spidev-systemd \
"

SRCREV = "2b3845a8d55c25e1c3a1942278d7fc050e4c9869"
SRC_URI = " \
	git://gitlab.com/ioplay/iop-center.git;protocol=https \
	file://rfid-player.service \
	file://boot.ogg \
"

S = "${WORKDIR}/git/"

inherit cmake_qt5 systemd

SYSTEMD_PACKAGES = "rfid-player"
SYSTEMD_SERVICE_rfid-player = "rfid-player.service"

SETTINGSDIR = "${localstatedir}/lib/rfid-player"
MEDIADIR = "${localstatedir}/local/media"
JINGLEDIR = "${datadir}/rfid-player/jingles"

do_install_append() {
  install -d ${D}${systemd_unitdir}/system/
  install -m 0644 ${WORKDIR}/rfid-player.service ${D}${systemd_unitdir}/system

  install -d ${D}${SETTINGSDIR}/matchplay

  SETTINGSFILE=${SETTINGSDIR}/matchplay/settings
  touch ${D}${SETTINGSFILE}

  install -d -m 0777 ${D}${MEDIADIR}
  echo "root-directory=file://${MEDIADIR}" >> ${D}${SETTINGSFILE}

  install -d ${D}${JINGLEDIR}
  install -m 0644 ${WORKDIR}/boot.ogg ${D}${JINGLEDIR}
  echo "started-file=file://${JINGLEDIR}/boot.ogg" >> ${D}${SETTINGSFILE}
}

FILES_rfid-player += " \
  ${bindir}/rfid-player \
  ${libdir}/rfid-player/* \
  ${base_libdir}/systemd/system/${SYSTEMD_SERVICE_rfid-player} \
  ${SETTINGSDIR} \
  ${MEDIADIR} \
  ${JINGLEDIR} \
"

FILES_test-player += " \
  ${bindir}/test-player \
  ${libdir}/test-player/* \
"

FILES_rfid-reader += " \
  ${bindir}/rfid-reader \
  ${libdir}/rfid-reader/* \
"

