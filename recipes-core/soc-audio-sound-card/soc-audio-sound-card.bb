DESCRIPTION = "delay sound.target until soc audio sound card is available"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/AGPL-3.0;md5=73f1eb20517c55bf9493b7dd6e480788"

SRC_URI = " \
  file://soc-audio-sound-card.target \
"

inherit systemd allarch

SYSTEMD_SERVICE_${PN} = "soc-audio-sound-card.target"

do_install_append() {
  install -D -m 0644 ${WORKDIR}/soc-audio-sound-card.target ${D}${systemd_unitdir}/system/soc-audio-sound-card.target
}

