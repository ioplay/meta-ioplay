inherit packagegroup

RDEPENDS_${PN} = " \
	alsa-utils \
	binutils \
	devmem2 \
	evtest \
	file \
	gcc-sanitizers \
	gdb \
	gdbserver \
	grep \
	gstreamer1.0 \
	gstreamer1.0-omx \
	gstreamer1.0-plugins-bad \
	gstreamer1.0-plugins-base \
	gstreamer1.0-plugins-good \
	gstreamer1.0-plugins-ugly \
	htop \
	i2c-tools \
	libgpiod \
	libgpiod-tools \
	libubsan \
	linux-firmware \
	mpc \
	mpd \
	netcat \
	perf \
	python3 \
	qtbase \
	qtimageformats \
	qtmultimedia \
	rsync \
	screen \
	socat \
	spitools \
	strace \
	tar \
	time \
	udev-extra-rules \
	usbutils \
	wget \
	wpa-supplicant \
	wpa-supplicant-passphrase \
"

