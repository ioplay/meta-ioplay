SUMMARY = "A small image just capable of allowing a device to boot."
LICENSE = "MIT"

inherit core-image

IMAGE_FEATURES += " \
	package-management \
"

# Include modules in rootfs
IMAGE_INSTALL += " \
	kernel-modules \
	packagegroup-core-boot \
	packagegroup-core-ssh-openssh \
	iop-center \
	rfid-player \
	test-player \
	rfid-reader \
"

