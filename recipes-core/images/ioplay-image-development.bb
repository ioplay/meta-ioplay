include ioplay-image.inc

IMAGE_ROOTFS_SIZE ?= "65536"

inherit populate_sdk_qt5
TOOLCHAIN_HOST_TASK += " \
	nativesdk-cmake \
"

IMAGE_FEATURES += " \
	debug-tweaks \
"

EXTRA_IMAGE_FEATURES += " \
	tools-debug \
"

IMAGE_INSTALL += " \
	packagegroup-ioplay-development \
	${CORE_IMAGE_EXTRA_INSTALL} \
"

