DESCRIPTION = "udev rule to tag spidev as with systemd to allow dependencies from services to spidev device"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/AGPL-3.0;md5=73f1eb20517c55bf9493b7dd6e480788"

SRC_URI = " \
  file://95-spidev-systemd.rules \
"

inherit allarch

do_install_append() {
	install -D -m0644 ${WORKDIR}/95-spidev-systemd.rules ${D}${sysconfdir}/udev/rules.d/95-spidev-systemd.rules
}

