LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/AGPL-3.0;md5=73f1eb20517c55bf9493b7dd6e480788"

inherit allarch

do_install () {
	install -d ${D}/${sysconfdir}
	echo "LANG=en_US.UTF-8" >> ${D}/${sysconfdir}/locale.conf
	echo "LANGUAGE=en" >> ${D}/${sysconfdir}/locale.conf
}

