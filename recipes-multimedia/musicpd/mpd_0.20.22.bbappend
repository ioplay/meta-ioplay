FILESEXTRAPATHS_prepend := "${THISDIR}/mpd:"

PACKAGECONFIG += " \
	alsa \
	ao \
	bzip2 \
	daemon \
	flac \
	libsamplerate \
	mpg123 \
	modplug \
	vorbis \
	zlib \
"

do_install_append() {
	install -o mpd -d ${D}/${localstatedir}/local/playlists
	install -m775 -o mpd -g mpd -d ${D}/${localstatedir}/local/music

  sed -i -e 's/After=network.target sound.target/#After=network.target\nAfter=sound.target/g' ${D}${base_libdir}/systemd/system/mpd.service
}
