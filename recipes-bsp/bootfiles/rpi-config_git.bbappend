
do_deploy_append() {
    if [ "${ENABLE_AUDIO}" = "1" ]; then
        echo "# Enable audio (loads snd_bcm2835)" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
        echo "dtparam=audio=on" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    fi
}

ENABLE_AUDIO = "1"
ENABLE_SPI_BUS = "1"

